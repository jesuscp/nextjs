import { Component } from "react";
import Link from "next/link";
import Header from "../components/header";

class AboutPage extends Component {
  static getInitialProps() {
    const valor = "Nueva página !";
    return { valor };
  }

  render() {
    return (
      <main>
        <Header />
        <section>
          <p>
            Estás en... 
            <strong>{this.props.valor}</strong>.
          </p>
          <Link href="/">
            <a>Regresar</a>
          </Link>
        </section>
      </main>
    );
  }
}

export default AboutPage;